#include <stdio.h>

#include "testCommon.h"

void testAbs() {
    testStart("abs");
    assertEqualsInt(abs(-1), 1);
}
/*
void testCreatePieceString() {
    piece p = {1, 0, LION, pl1[0], "△L", 0};
    char line[100];
    createPieceString(&p, line);
    assertEqualsString(line, "1:0:LION:-1:△L:0");
}
*/
int main() {
    testAbs();
    // testCreatePieceString();
    testErrorCheck();
}
