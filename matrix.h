#ifndef __MATRIX_H
#define __MATRIX_H

#define N 100
#define X 3
#define Y 4

typedef enum { LION, ELEPHANT, GIRAFFLE, CHICK, HEN } pieceTypeE;

typedef struct {
    int direction;  // {-1 or 1}
    char name[255];
} player;

typedef struct {
    int x;
    int y;
    pieceTypeE type;
    player *playerp;
    char pieceString[10];
    int num;
} piece;

typedef struct {
    int ban[X][Y];
    int dai[2][7];
    piece koma[8];
} board;

void initBoard(board *brd);
void display(board *brd);
#endif