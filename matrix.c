#include "matrix.h"

#include <stdio.h>

void initBoard(board *brd) {
    //
    player pl[2];
    pl[0].direction = -1;
    pl[1].direction = 1;
    piece initPieces[8] = {
        {1, 0, LION, &pl[0], "L(▽ )", 0},
        {0, 0, GIRAFFLE, &pl[0], "G(▽ )", 1},
        {2, 0, ELEPHANT, &pl[0], "E(▽ )", 2},
        {1, 1, CHICK, &pl[0], "C(▽ )", 3},
        {1, 3, LION, &pl[1], "L(△ )", 4},
        {0, 3, ELEPHANT, &pl[1], "E(△ )", 5},
        {2, 3, GIRAFFLE, &pl[1], "G(△ )", 6},
        {1, 2, CHICK, &pl[1], "C(△ )", 7},
    };  // 初期状態の駒データ
    // bank *pp;
    // pp->masu[3][4] = {{1, -1, -1, 5}, {0, 3, 7, 4}, {2, -1, -1, 6}};
    // brd->ban.masu[0][0] = pp.masu;

    for (int i = 0; i < X; i++) {  //空白で埋める
        for (int j = 0; j < Y; j++) {
            brd->ban[i][j] = -1;
        }
    }
    for (int i = 0; i < 2; i++) {  //空白で埋める
        for (int j = 0; j < 7; j++) {
            brd->dai[i][j] = -1;
        }
    }
    for (int i = 0; i < 8; i++) {
        brd->ban[initPieces[i].x][initPieces[i].y] = initPieces[i].num;
        brd->koma[i] = initPieces[i];
    }
}

void display(board *brd) {
    //それぞれのマス毎に文字を割り振る。
    char *bangoma[X][Y][10];
    char nullBangoma[10] = ".....";
    for (int i = 0; i < X; i++) {
        for (int j = 0; j < Y; j++) {
            if (brd->ban[i][j] == -1) {
                bangoma[i][j][9] = nullBangoma;
            } else {
                piece *bangomap = brd->koma + brd->ban[i][j];
                bangoma[i][j][9] = bangomap->pieceString;
            }
        }
    }

    //それぞれの駒台に文字を割り振る
    char *motigoma[2][7][10];
    char nullMotigoma[10] = "";
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 7; j++) {
            if (brd->dai[i][j] == -1) {
                motigoma[i][j][9] = nullMotigoma;
            } else {
                piece *motigomap = brd->koma + brd->dai[i][j];
                motigoma[i][j][9] = motigomap->pieceString;
            }
        }
    }

    fprintf(stderr, "\nPlayer Aの持ち駒:\n");
    for (int i = 0; i < 7; i++) {
        fprintf(stderr, "%s ", motigoma[0][i][9]);
    }
    fprintf(stderr, "\n\n");

    fprintf(stderr, "    0     1     2  \n");
    fprintf(stderr, " -------------------\n");
    for (int j = 0; j < Y; j++) {
        fprintf(stderr, "%d|%s|%s|%s|\n", j, bangoma[0][j][9], bangoma[1][j][9],
                bangoma[2][j][9]);
    }
    fprintf(stderr, " -------------------\n");

    fprintf(stderr, "\nPlayer Bの持ち駒:\n");
    for (int i = 0; i < 7; i++) {
        fprintf(stderr, "%s ", motigoma[1][i][9]);
    }
    fprintf(stderr, "\n\n");
}